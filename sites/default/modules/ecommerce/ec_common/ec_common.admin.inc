<?php
// $Id: ec_common.admin.inc,v 1.4.2.3 2009/03/02 04:26:41 gordon Exp $

/**
 * @file
 * This module contains generic functions for ecommerce which are typically
 * available to the user with 'store admin manage' permissions.
 */

/**
 * Create an overview of the Settings Page (based on system_settings_overview).
 */
function ec_common_settings_overview($path) {
  $content = system_admin_menu_block(menu_get_item($path));
  return theme('admin_block_content', $content);
}

