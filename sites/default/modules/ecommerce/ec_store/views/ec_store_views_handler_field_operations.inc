<?php
// $Id: ec_store_views_handler_field_operations.inc,v 1.1.2.3 2010/05/30 13:19:34 gordon Exp $

/**
 * @file
 * Provide list of operation links for transactions.
 */

class ec_store_views_handler_field_operations extends views_handler_field {

  function render($values) {
    if ($txn = ec_store_transaction_load($values->{$this->field_alias})) {
      drupal_add_css(drupal_get_path('module', 'ec_store') . '/css/views.css');
      return theme('links', module_invoke_all('link', 'ec_store_transaction', $txn), array('class' => 'links ec-store-views-operations'));
    }
  }
}
