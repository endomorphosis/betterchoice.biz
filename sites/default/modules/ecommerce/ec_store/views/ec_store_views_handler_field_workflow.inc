<?php
// $Id: ec_store_views_handler_field_workflow.inc,v 1.1.2.4 2009/07/02 10:25:39 recidive Exp $

/**
 * @file
 * Provide output of workflow status descriptions.
 */

class ec_store_views_handler_field_workflow extends views_handler_field {
  function sort_clickable($order) {
    $this->query->add_orderby('ec_workflow_statuses', 'weight', $order);
  }

  function render($values) {
    return drupal_ucfirst($values->{$this->field_alias});
  }
}

