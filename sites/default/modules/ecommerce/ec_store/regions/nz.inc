<?php
// $Id: nz.inc,v 1.1.2.1 2010/11/21 06:20:08 gordon Exp $

/**
 * @file
 * New Zealand regional Settings
 */

/**
 * Implementation of hook_ec_region_COUNTRYCODE_info().
 */
function ec_store_ec_region_nz_info() {
  return array(
    // Geo
    'state_name' => t('Province'),
    'use_state_names' => TRUE,
    'zip' => TRUE,
    'zip_name' => t('Postcode'),
  );
}