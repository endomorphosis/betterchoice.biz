<?php
// $Id: ca.inc,v 1.1.2.2 2010/12/16 11:55:58 gordon Exp $

/**
 * @file
 */

/**
 * Implementation of hook_ec_region_COUNTRYCODE_info().
 */
function ec_store_ec_region_ca_info() {
  return array(
    // Measures
    'weight' => 'KG',
    'length' => 'M',
    'area' => 'M2',
    'volume' => 'M3',
    // Geo
    'state_name' => t('Province'),
    'use_state_names' => TRUE,
    'zip' => TRUE,
    // Price format
    'payment_symbol' => '$',
    'payment_symbol_position' => 1, // Left
    'payment_thousands' => ',',
    'payment_decimal' => '.',
    'payment_decimal_places' => 2,
  );
}