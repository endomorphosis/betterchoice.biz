<?php
// $Id: ec-cart-display-item.tpl.php,v 1.1.2.4 2010/12/16 11:55:57 gordon Exp $

/**
 * @file
 */
?>
<div id="ec-cart-block-item-<?php echo $nid; ?>" <?php echo drupal_attributes($attributes); ?>>
  <div class="ec-cart-item">
    <?php echo $link . $qty_multiplier . $qty; ?>
  </div>
  <div class="ec-cart-line-total">
    <?php echo $price; ?>
  </div>
</div>