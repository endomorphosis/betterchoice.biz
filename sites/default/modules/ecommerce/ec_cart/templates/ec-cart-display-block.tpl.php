<?php
// $Id: ec-cart-display-block.tpl.php,v 1.1.2.5 2010/12/16 11:55:57 gordon Exp $

/**
 * @file
 */
?>
<div class="ec-item-count">
  <?php echo $item_count; ?>
</div>
<div class="ec-cart-items">
  <div class="items">
    <?php echo implode("\n", $items); ?>
  </div>
  <div class="total-wrapper clear-block">
    <span class="total">
      <?php echo $total; ?>
    </span>
  </div>
  <div class="ec-checkout">
    <?php echo $checkout; ?>
  </div>
</div>
