<?php
// $Id: ec-product-admin-features-overview.tpl.php,v 1.1.2.2 2010/12/16 11:55:58 gordon Exp $

/**
 * @file
 */
?>
<div id="ec-product-feature-list-wrapper">
  <div class="ec-product-feature-list">
    <?php print $feature_list; ?>
  </div>
  <hr />
  <div class="ec-product-feature-add">
    <h3><?php print t('Add feature'); ?></h3>
    <?php print $feature_add; ?>
  </div>
</div>