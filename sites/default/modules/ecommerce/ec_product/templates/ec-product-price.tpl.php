<?php
// $Id: ec-product-price.tpl.php,v 1.1.2.2 2010/12/16 11:55:58 gordon Exp $

/**
 * @file
 */
?>
<div class="price">
  <strong><?php echo $price_prefix; ?></strong> <?php echo $price; ?>
</div>
