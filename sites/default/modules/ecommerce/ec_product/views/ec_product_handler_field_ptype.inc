<?php
// $Id: ec_product_handler_field_ptype.inc,v 1.1.2.4 2009/10/18 14:18:21 davea Exp $

/**
 * @file
 * Implements display handler for product types.
 */

class ec_product_handler_field_ptype extends views_handler_field {
  function render($values) {
    if (isset($values->{$this->field_alias})) {
      $value = ec_product_get_types('name', $values->{$this->field_alias});
      return check_plain($value);
    }
  }
}

