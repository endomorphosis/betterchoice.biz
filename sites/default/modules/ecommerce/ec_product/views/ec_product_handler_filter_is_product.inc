<?php
// $Id: ec_product_handler_filter_is_product.inc,v 1.1.2.2 2009/07/02 10:25:40 recidive Exp $

/**
 * @file
 * Provide filter for product nodes.
 */

class ec_product_handler_filter_is_product extends views_handler_filter_boolean_operator {
  function query() {
    $table = $this->ensure_my_table();
    if ($this->value) {
      $this->query->add_where($this->options['group'], "$table.ptype IS NOT NULL");
    }
    else {
      $this->query->add_where($this->options['group'], "$table.ptype IS NULL");
    }
  }
}
