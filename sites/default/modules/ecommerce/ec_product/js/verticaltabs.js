Drupal.verticalTabs = Drupal.verticalTabs || {};

Drupal.verticalTabs.product = function() {
  var text = $('#edit-ptype-description').val();
  return text ? text : Drupal.t('Not a product');
}