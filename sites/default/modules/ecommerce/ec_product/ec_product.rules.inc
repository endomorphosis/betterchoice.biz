<?php
// $Id: ec_product.rules.inc,v 1.1.2.2 2009/07/02 10:25:39 recidive Exp $

/**
 * @file
 * Rules implementation for ec_product module.
 */

/**
 * Implementation of hook_rules_event_info().
 */
function ec_product_rules_event_info() {
  return array(
    'transaction_product_save' => array(
      'label' => t('Transaction Product Save'),
      'module' => 'eC Product',
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Altered Transaction')),
        'original' => array('type' => 'transaction', 'label' => t('Original Transaction')),
        'item' => array('type' => 'node', 'label' => t('Transaction Item')),
      ),
    ),
  );
}

