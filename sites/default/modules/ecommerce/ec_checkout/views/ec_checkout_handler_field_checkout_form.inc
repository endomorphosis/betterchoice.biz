<?php
// $Id: ec_checkout_handler_field_checkout_form.inc,v 1.1.2.5 2010/08/15 06:46:10 gordon Exp $

/**
 * @file
 * Implements display handler for checkout form.
 */

class ec_checkout_handler_field_checkout_form extends views_handler_field {
  function render($values) {
    $node = node_load($values->{$this->field_alias});

    if (isset($node->ptype) && ec_product_can_purchase($node)) {
      return drupal_get_form('ec_checkout_product_form', $node, TRUE);
    }
  }
}
