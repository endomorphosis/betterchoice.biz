<?php
// $Id: ec_anon.install,v 1.10.2.22 2010/12/16 11:55:57 gordon Exp $

/**
 * @file
 * Install and uninstall routines, incremental database updates and database
 * schema.
 */

/**
 * Implementation of hook_install().
 */
function ec_anon_install() {
  if (db_table_exists('ec_product') && !db_column_exists('ec_product', 'anon_policy')) {
    $ret = array();
    db_add_field($ret, 'ec_product', 'anon_policy', array(
      'type' => 'int',
      'size' => 'tiny',
      'not null' => TRUE,
      'default' => 2,
    ));
  }
}

/**
 * Implementation of hook_uninstall().
 */
function ec_anon_uninstall() {
  if (db_table_exists('ec_product') && db_column_exists('ec_product', 'anon_policy')) {
    $ret = array();
    db_drop_field($ret, 'ec_product', 'anon_policy');
  }
}

/**
 * Implementation of hook_schema_alter().
 */
function ec_anon_schema_alter(&$schema) {
  $schema['ec_product']['fields']['anon_policy'] = array(
    'type' => 'int',
    'size' => 'tiny',
    'not null' => TRUE,
    'default' => 2
  );
}

/**
 * Implementation of hook_enable().
 */
function ec_anon_enable() {
  $plist = ec_anon_policy_list(FALSE);
  drupal_set_message(st('Anonymous purchasing policy for the site has been set to <b>!policy</b>. You can change it on the <a href="@settingsurl">store settings page</a>.',
    array('!policy' => $plist[variable_get('ec_anon_policy', ECANON_POLICY_OPTIONAL)], '@settingsurl' => url('admin/ecsettings'))));

  // Set the policy variable.
  $oldval = variable_get('store_auth_cust', FALSE);
  if ($oldval === FALSE) {
    $newval = ECANON_POLICY_DEFAULT;
  }
  elseif (!empty($oldval)) {
    $newval = ECANON_POLICY_NEVER;
  }
  else {
    $newval = ECANON_POLICY_OPTIONAL;
  }
  variable_set('ec_anon_policy', $newval);

  // Reset the the ptypes cache so it loads up the new ptypes correctly
  if (function_exists('ec_product_get_types')) {
    ec_product_get_types('types', NULL, TRUE);
  }
}

/**
 * Implementation of hook_update_n().
 */
function ec_anon_update_4001() {
  drupal_install_modules(array('ec_common'));
  if (!function_exists('ec_anon_policy_list')) {
    drupal_load('module', 'ec_anon');
  }
  ec_anon_enable();
  return array();
}

/**
 * Remove all the anonymous product features from the product types
 */
function ec_anon_update_6401() {
  if (module_exists('ec_product')) {
    foreach (ec_product_get_types() as $ptype => $info) {
      ec_product_feature_disable($info, 'anonymous');
    }
  }

  return array();
}

/**
 * Move the ec_anon.policy to ec_product.anon_policy
 */
function ec_anon_update_6402() {
  $ret = array();

  if (db_table_exists('ec_product') && !db_column_exists('ec_product', 'anon_policy')) {
    db_add_field($ret, 'ec_product', 'anon_policy', array(
      'type' => 'int',
      'size' => 'tiny',
      'not null' => TRUE,
      'default' => 2,
    ));
  }

  return $ret;
}

/**
 * Move over all of the records to the ec_product table
 */
function ec_anon_update_6403(&$sandbox) {
  $ret = array();

  if (empty($sandbox)) {
    $sandbox['total'] = db_result(db_query('SELECT MAX(vid) FROM {ec_anon}'));
    $sandbox['last'] = 0;
  }

  $result = db_query('SELECT vid, policy AS anon_policy FROM {ec_anon} WHERE vid > %d ORDER BY vid ASC', $sandbox['last']);
  while ($row = db_fetch_array($result)) {
    drupal_write_record('ec_product', $row, 'vid');

    $sandbox['last'] = $row['vid'];
    if (timer_read('batch_processing') > 10000) {
      break;
    }
  }

  if ($sandbox['total'] <= $sandbox['last']) {
    $ret['#finished'] = 1;
  }
  else {
    $ret['#finished'] = $sandbox['last']/$sandbox['total'];
  }

  return $ret;
}

/**
 * Move the ec_anon.policy to ec_product.anon_policy
 */
function ec_anon_update_6404() {
  $ret = array();

  if (db_table_exists('ec_product') && !db_column_exists('ec_product', 'anon_policy')) {
    db_add_field($ret, 'ec_product', 'anon_policy', array(
      'type' => 'int',
      'size' => 'tiny',
      'not null' => TRUE,
      'default' => 2,
    ));
  }

  return $ret;
}

/**
 * Drop the ec_anon table as it is not required any more.
 */
function ec_anon_update_6406() {
  $ret = array();

  if (db_table_exists('ec_anon')) {
    db_drop_table($ret, 'ec_anon');
  }

  return $ret;
}