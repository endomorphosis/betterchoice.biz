<?php
// $Id: ec-address-user-list.tpl.php,v 1.1.2.2 2010/12/16 11:55:57 gordon Exp $
/**
 * @file
 * User address list template
 */
?>
<div id="ec-address-list-wrapper">
  <div class="ec-address-list">
    <?php print $address_list; ?>
  </div>
  <hr />
  <div class="ec-address-form">
    <h3><?php print t('Add address'); ?></h3>
    <?php print $address_form; ?>
  </div>
</div>
