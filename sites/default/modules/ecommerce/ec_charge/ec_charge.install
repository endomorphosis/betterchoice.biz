<?php
// $Id: ec_charge.install,v 1.1.4.17 2010/12/16 11:55:57 gordon Exp $

/**
 * @file
 * Install and uninstall routines, incremental database updates and database
 * schema.
 */

/**
 * Implementation of hook_install().
 */
function ec_charge_install() {
  drupal_install_schema('ec_charge');
}

/**
 * Implementation of hook_install().
 */
function ec_charge_uninstall() {
  drupal_uninstall_schema('ec_charge');
}

/**
 * Implementation of hook_schema().
 */
function ec_charge_schema() {
  $schema['ec_charge'] = array(
    'description' => t('Store of miscellenous charges'),
    'export' => array(
      'key' => 'name',
      'can disable' => FALSE,
      'status' => 'enabled',
    ),
    'fields' => array(
      'chgid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'disp-width' => '11',
        'description' => t('Primary Key: Unique ID for each charge'),
        'no export' => TRUE,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'description' => t('Unique identifier for each charge'),
      ),
      'chg_type' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Indicates the charge type, transaction/product'),
      ),
      'chg_group' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 16,
        'default' => '',
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => t('Description of the charge that will appear on the transaction'),
      ),
      'filters' => array(
        'type' => 'text',
        'size' => 'medium',
        'serialize' => TRUE,
      ),
      'variables' => array(
        'type' => 'text',
        'size' => 'medium',
        'serialize' => TRUE,
      ),
      'calculations' => array(
        'type' => 'text',
        'size' => 'medium',
        'serialize' => TRUE,
      ),
      'modifications' => array(
        'type' => 'text',
        'size' => 'medium',
        'serialize' => TRUE,
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Weight of the charge to determine the order'),
      ),
      'enabled' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => t('Set the charged as enabled.'),
      ),
    ),
    'primary key' => array('chgid'),
  );

  return $schema;
}

/**
 * Rename componet to component in all tables and columns.
 */
function ec_charge_update_6401() {
  $ret = array();

  db_rename_table($ret, 'ec_charge_componets', 'ec_charge_components');
  db_change_field($ret, 'ec_charge_components', 'componet', 'component', array('type' => 'varchar', 'length' => '16', 'not null' => TRUE));

  return $ret;
}

/**
 * Add type field to ec_charge table
 */
function ec_charge_update_6402() {
  $ret = array();

  db_add_field($ret, 'ec_charge', 'type',
    array(
      'type' => 'int',
      'not null' => TRUE,
      'length' => 1,
      'default' => 0,
      'description' => t('Indicates the charge type, transaction/product'),
    )
  );

  db_add_field($ret, 'ec_charge', 'chg_group',
    array(
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => 16,
      'default' => '',
    )
  );

  return $ret;
}

/**
 * Add new parent field
 */
function ec_charge_update_6403() {
  $ret = array();

  db_add_field($ret, 'ec_charge_components', 'parent', array(
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE,
    'default' => 0,
  ));

  return $ret;
}

/**
 * Add additional fields to allow for ctools support
 */
function ec_charge_update_6404() {
  $ret = array();

  db_add_field($ret, 'ec_charge', 'filters', array(
    'type' => 'text',
    'size' => 'medium',
    'serialize' => TRUE,
  ));
  db_add_field($ret, 'ec_charge', 'variables', array(
    'type' => 'text',
    'size' => 'medium',
    'serialize' => TRUE,
  ));
  db_add_field($ret, 'ec_charge', 'calculations', array(
    'type' => 'text',
    'size' => 'medium',
    'serialize' => TRUE,
  ));
  db_add_field($ret, 'ec_charge', 'modifications', array(
    'type' => 'text',
    'size' => 'medium',
    'serialize' => TRUE,
  ));

  return $ret;
}

/**
 * Port all the charges to the new serialized format
 */
function ec_charge_update_6405() {
  $ret = array();

  if (db_table_exists('ec_charge_components')) {
    $result = db_query('SELECT name from {ec_charge}');

    while ($chg = db_fetch_object($result)) {
      if ($chg = ec_charge_load($chg->name)) {
        foreach (array('filters', 'variables', 'calculations', 'modifications') as $component) {
          $chg->{$component} = array();
        }

        $components = db_query('SELECT c.*, IF(c.parent, p.weight, c.weight) AS parent_weight FROM {ec_charge_components} c LEFT JOIN {ec_charge_components} p ON c.parent = p.id WHERE c.chgid = %d ORDER BY parent_weight ASC, c.parent ASC, c.weight ASC', $chg->chgid);
        while ($row = db_fetch_array($components)) {
          if ($row['data']) {
            $data = unserialize($row['data']);
            if (is_array($data)) {
              $row+= $data;
            }
            unset($row['data']);
          }
          $chg->{$row['type'] .'s'}[$row['id']] = $row;
        }
        ec_charge_save($chg);
      }
    }
  }
  return $ret;
}

/**
 * remove the {ec_charge_components} table which is no longer used.
 */
function ec_charge_update_6406() {
  $ret = array();

  db_drop_table($ret, 'ec_charge_components');

  return $ret;
}

/**
 * Rename type to chg_type
 */
function ec_charge_update_6407() {
  $ret = array();

  db_change_field($ret, 'ec_charge', 'type', 'chg_type', array(
    'type' => 'int',
    'not null' => TRUE,
    'default' => 0,
    'description' => t('Indicates the charge type, transaction/product'),
  ));

  return $ret;
}