<?php
// $Id: ec-charge-add-component.tpl.php,v 1.1.2.2 2010/12/16 11:55:57 gordon Exp $

/**
 * @file
 */
?>
<div id="ec-charge-categories">
  <div id="ec-charge-categories-wrapper">
    <?php echo $categories; ?>
  </div>
</div>
<div id="ec-charge-components">
  <div id="ec-charge-compaonets-wrapper">
    <?php echo $components; ?>
  </div>
</div>