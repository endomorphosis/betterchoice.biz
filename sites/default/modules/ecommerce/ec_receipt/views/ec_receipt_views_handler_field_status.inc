<?php
// $Id: ec_receipt_views_handler_field_status.inc,v 1.1.2.1 2009/07/02 10:25:39 recidive Exp $

/**
 * @file
 * Provides the rendering of the receipt status field.
 */

class ec_receipt_views_handler_field_status extends views_handler_field {
  function render($values) {
    return ec_receipt_get_status($values->{$this->field_alias});
  }
}

