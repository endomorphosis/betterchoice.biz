<?php 
include('db_connect.php');

function tstamptotime($tstamp) {
  // converts ISODATE to unix date
  // 1984-09-01T14:21:31Z
  sscanf($tstamp,"%u-%u-%uT%u:%u:%uZ",$year,$month,$day,
  $hour,$min,$sec);
  $newtstamp=mktime($hour,$min,$sec,$month,$day,$year);
  return $newtstamp;
    }     

function check_price($query){
// Find The price and echo the json results as quickly as possible, and return the results of the function.
$check_price_query = "SELECT * FROM `keywords` WHERE `keyword` = '$query'";
$price_query = mysql_query($check_price_query);
  if($keyword_result = mysql_fetch_array($price_query)){
    $price = $keyword_result['price'];
      if(!empty($price) && $price > 0){
      $results['keywords'][0]['name'] = $query;
      $results['keywords'][0]['price'] = $price;
      echo(json_encode($results));
      }
      else{
      //if no suitable price is found, try to update to get the price
      $json = update_keywords($query);
      $price = json_decode($json);
      $check_json_price = $price['keywords'][0]['price'];
	 if(!empty($check_json_price) && $check_json_price > 0){
	 $results = $price;
	 echo($json);
	 }
	 else{
	 $results['error'] = "<h3> could not locate price </h3>";
	 echo(json_encode($results));
	 }
      }
  }
  else{
  //if no keyword is found then add it to the keywords list
  $results['error'] = "<h3> could not locate keyword ".mysql_error()."</h3>";
  echo(json_encode($results));
  update_auction_keyword($query);
  }
return json_encode($results);  
}               

//Create Average Price 
function harmonic () {
  $num_args = func_num_args ();
  for ($i = 0; $i < $num_args; $i++) {
      $sum += 1 / func_get_arg ($i);
  }
  return $num_args / $sum;
}


//This should be ran on some sort of cron job
function update_keywords($query){

// Find all auctions that ended before tomorrow
$timestamp = new DateTime("now");
$today =  $timestamp->format('U');
$tomorrow = $timestamp->add(date_interval_create_from_date_string('1 days'));
$end_date = $tomorrow->format('U');

  // Find out if there is a specific keyword to update, or updating all keywords
  if(empty($query) || $query == ''){
  $query1 = "SELECT * FROM  `keywords` ORDER BY `searches` DESC ";
  $find_keywords = mysql_query("SELECT * FROM  `keywords` ORDER BY `searches` DESC ");$find_auction_items = mysql_query("SELECT * from `auctions` WHERE `end_date` < ".$end_date." AND `ended` != 'Y'");
  }
  else{
  $query1 = "SELECT * FROM  `keywords` WHERE `keyword` = '$query' ";
  $find_keywords = mysql_query("SELECT * FROM  `keywords` WHERE `keyword` = '$query' ");
  $find_auction_items = mysql_query("SELECT * from `auctions` WHERE `keywords` LIKE  '%\"$query\"%' AND `end_date` < ".$end_date." AND `ended` != 'Y'");
  }
  // Create array of all auction items
  while($items = mysql_fetch_array($find_auction_items)){
  $item_ids[] =  $items['auction_id'];
  }
    // Group the itemss for the max query of 20, and update the prices 
    for($i = 0; $item_ids[$i]; $i++){
    $item_list[] = $item_ids[$i];
      if($i % 20 == 0){
      update_auction_prices($item_list);
      unset($item_list);
      }
    }
    update_auction_prices($item_list);
  


//find the timestamp for 90 days ago
$dateB = new DateTime("now"); 
$dateA = $dateB->sub(date_interval_create_from_date_string('90 days'));
$ninety_days = $dateA->format('U');
  
  while($keyword = mysql_fetch_array($find_keywords)){
    //Find the auction(s)
    $current_keyword = $keyword['keyword'];
  //  print $current_keyword;
    $item_matching_query = "SELECT * FROM  `auctions` WHERE  `keywords` LIKE  '%\"$current_keyword\"%' AND `end_date` > $ninety_days AND `ended` = 'Y'";
    $items_matching_keywords = mysql_query($item_matching_query);
    $count_items = 0;
      //Create an array of prices for the auctions, and an array of items
      while($results = mysql_fetch_array($items_matching_keywords)){
	$items[] = $results;
	$count_items++;
	$prices[] = $results['price'];
      }
      if(empty($items)){
      $results['error'] .= '<h3> Could not find that auctions for that keyword in the last 90 days</h3>';
      }

    //find the average price, and update the keyword list with the price
    $harmonic_average = harmonic(implode($prices, ', '));
    $update_query = "UPDATE `keywords` SET `price` = '$harmonic_average' , `items` = '$count_items' WHERE `keyword` = '$current_keyword' ";
    $update_average = mysql_query($update_query);
    unset($prices);
    update_auction_keyword($current_keyword);
    $keyword_prices['name'] = $current_keyword;
    $keyword_prices['price'] = $harmonic_average;
    $keyword_results[] = $keyword_prices;
  }
  $results['keywords'] =  $keyword_results;
  // If update proces fails, try to add the query to the keyword search
  if(empty($item_ids)){
  $results['error'] .= "<h3> No Items with the Keyword were found! ".mysql_error()."</h3>";
  }
  if(empty($current_keyword)){
  $results['error'] .= "<h3> Could not find that keyword  ".mysql_error()."</h3>";
  }
  if((empty($current_keyword) || empty($item_ids)) && (!empty($query) && $query != '')){  
  update_auction_keyword($query);
  }

// Return results
return json_encode($results);
}

function buildURLArray ($filterarray) {
  global $urlfilter;
  global $i;
  $i = 0;
  // Iterate through each filter in the array
  foreach($filterarray as $itemfilter) {
    // Iterate through each key in the filter
    foreach ($itemfilter as $key =>$value) {
      if(is_array($value)) {
        foreach($value as $j => $content) { // Index the key for each value
          $urlfilter .= "&itemFilter($i).$key($j)=$content";
        }
      }
      else {
        if($value != "") {
          $urlfilter .= "&itemFilter($i).$key=$value";
        }
      }
    }
    $i++;
  }
  return "$urlfilter";
} // End of buildURLArray function

//Update ebay listings in the database
function update_auction_keyword($query){

// API request variables
$endpoint = 'http://svcs.ebay.com/services/search/FindingService/v1';  // URL to call
$version = '1.0.0';  // API version supported by your application
$appid = 'junkeez71-f54d-4069-af89-729a1eb2d45';  // Replace with your own AppID
$globalid = 'EBAY-US';  // Global ID of the eBay site you want to search (e.g., EBAY-DE)
  if(!empty($query)){
  $safequery = urlencode($query);  // Make the query URL-friendly

  $i = '0';  // Initialize the item filter index to 0

  // Create a PHP array of the item filters you want to use in your request
  $filterarray =
    array(
      array(
      'name' => 'Condition',
      'value' => array('7000'),
      'paramName' => '',
      'paramValue' => '')
    );

  // Generates an indexed URL snippet from the array of item filters


  // Build the indexed item filter URL snippet
  $urlfilter = buildURLArray($filterarray);
  // Construct the findItemsByKeywords HTTP GET call 
  $apicall = "$endpoint?";
  $apicall .= "OPERATION-NAME=findItemsAdvanced";
  $apicall .= "&SERVICE-VERSION=$version";
  $apicall .= "&SECURITY-APPNAME=$appid";
  $apicall .= "&GLOBAL-ID=$globalid";
  $apicall .= "&keywords=$safequery";
  $apicall .= "&paginationInput.entriesPerPage=200";
  $apicall .= "$urlfilter";


  //print $apicall;
  // Load the call and capture the document returned by eBay API
  $resp = simplexml_load_file($apicall);

    // Check to see if the request was successful, else print an error
    if ($resp->ack == "Success") {
      $results = '';
      // If the response was loaded, parse it and build links  
      foreach($resp->searchResult->item as $item) {
	$pic   = $item->galleryURL;
	$link  = $item->viewItemURL;
	$title = $item->title;
	$price = $item->sellingStatus->currentPrice;
	$duration = $item->sellingStatus->timeLeft;
	$end_time = $item->listingInfo->endTime;
	$end_time = tstamptotime($end_time);
	$itemid = $item->itemId;
	$type = $item->listingInfo->listingType;

	// For each SearchResultItem node, build a link and append it to $items and query auction
	$items[] = $item;

	$find_auction = mysql_query("SELECT * FROM `auctions` where `auction_id` = '$itemid'");
	$auction_result = mysql_fetch_array($find_auction);
	
	if(empty($auction_result)){
	  // Insert if it doesn't exist
	  unset($new_keywords);
	  $new_keywords[] = $query;
	  $insert_keywords = serialize($new_keywords);
	  $insert_auction = mysql_query("INSERT INTO `auctions` (`auction_id` , `keywords` , `auction_name`, `price`, `end_date`, `type`	 ) VALUES ( '$itemid', '$insert_keywords', '$title', '$price', '$end_time', '$type' )");
	}
	else{
	  //Otherwise grab keywords into array, and add if it is not in the array, add it. 
	  $auction_keywords = unserialize($auction_result['keywords']);
	  if(!empty($auction_keywords)){
	    if(!in_array($query,$auction_keywords)){
	      $auction_keywords[] =  $query;
	      $new_keywords = serialize($auction_keywords);
	      mysql_query("UPDATE `auctions` SET `keywords` = '$new_keywords' WHERE `auction_id` = '$itemid' ");
	    }
	  }
	}
      }
      // Look to see if search has been indexed, and if so add 1 to the number of searches
      $find_keyword = mysql_query("SELECT * FROM `keywords` where `keyword` = '$query'");
      $keyword_result = mysql_fetch_array($find_keyword);
      if(empty($keyword_result)){
	$insert_auction = mysql_query("INSERT INTO `keywords` (`keyword` , `searches`) VALUES ( '$query', '1')");
      }
      else{
	$searhes = $keyword_result['searches'] + 1;
	mysql_query("UPDATE `keywords` SET `searches` = '$searhes' WHERE `keyword` = '$query' ");
      }
      $results['items'] = $items;
    }
    // If not successfull print an error
    else {
      $results['error']  = "<h3>Oops! The request was not successful. Make sure you are using a valid AppID for the Production environment.</h3>";
    }
  }	
  else{
    $results['error']  = "<h3>There is no query</h3>";
  }
// Return Json of data from operation
return json_encode($results);
}


function update_auction_prices($item_array){
//Grab a set of auctions from an array of auction ID's
$endpoint = 'http://open.api.ebay.com/shopping';  // URL to call
$version = '1.0.0';  // API version supported by your application
$appid = 'junkeez71-f54d-4069-af89-729a1eb2d45';  // Replace with your own AppID
$globalid = 'EBAY-US';  // Global ID of the eBay site you want to search (e.g., EBAY-DE)
$query = implode($item_array, ',');  // Auction ID's
$safequery = urlencode($query);  // Make the query URL-friendly
$apicall = "$endpoint?";
$apicall .= "callname=GetMultipleItems";
$apicall .= "&version=525";
$apicall .= "&appid=$appid";
$apicall .= "&siteid=0";
$apicall .= "&responseencoding=XML";
$apicall .= "&IncludeSelector=Details"; 
$apicall .= "&ItemID=$safequery";


// Load the call and capture the document returned by eBay API
$resp = simplexml_load_file($apicall);
  // Check to see if the request was successful, else print an error
  if ($resp->Ack == "Success") {
    $results = $resp->Item;
    //Iterate through each item, and determine if the item was completed, and if it was also sold. Unsold items are deleted, while uncompleted items are left alone.
    foreach($resp->Item as $item){
      if($item->ListingStatus == "Completed"){
	$price = $item->ConvertedCurrentPrice;
	$itemid = $item->ItemID;
	if($item->QuantitySold > 0){
	  $update_query = mysql_query("UPDATE `auctions` SET `price` = '$price' , `ended` = 'Y' WHERE `auction_id` = '$itemid' ");
	}
	else{
	  $del_query_string = "DELETE FROM `auctions` WHERE `auction_id` = '$itemid'";
	  $delete_query = mysql_query($del_query_string);
	}
      }
    }
  }
}

?>